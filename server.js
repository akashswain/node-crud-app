require("dotenv").config();

const express = require("express");
const bcrypt = require("bcrypt");

const { authenticateToken } = require("./middleware");
const { responseGenerator } = require("./helper");
const { users, posts } = require("./database");

app = express();
app.use(express.json());

/* 
-----------------------------------
All API calls are listed below
-----------------------------------
*/

// create a new user
app.post("/users", async (req, res) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const user = { username: req.body.username, password: hashedPassword };

    users.push(user);
    successMsg = responseGenerator(false, user);
    res.status(201).send(successMsg);
  } catch (err) {
    errorMsg = responseGenerator(true, `Unable to create user - ${err}`);
    res.status(400).send(errorMsg);
  }
});

// get the list of all users
app.get("/users", authenticateToken, (req, res) => {
  successMsg = responseGenerator(false, users);
  res.json(successMsg);
});

// get the list of all posts created by a user
app.get("/posts", authenticateToken, (req, res) => {
  const filteredPosts = posts.filter((post) => {
    // console.log(post.username, req.user.username);
    return post.username === req.user.username;
  });
  successMsg = responseGenerator(false, filteredPosts);
  res.json(successMsg);
});

/* ---------------------
start server 
------------------------*/
app.listen(3000, () => {
  console.log("started server on localhost port 3000");
});
