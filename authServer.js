require("dotenv").config();

const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

app = express();
app.use(express.json());

const { users, refreshTokens } = require("./database");
const {
  generateJwtToken,
  responseGenerator,
  generateJwtTokenWithoutExpiration,
} = require("./helper");

/* 
-----------------------------------
All API calls are listed below
-----------------------------------
*/

// User login using username and password
app.post("/users/login", async (req, res) => {
  // dummy case till database is setup

  user = { username: req.body.username };
  const accessToken = generateJwtToken(user);
  const refreshToken = generateJwtTokenWithoutExpiration(user);
  refreshToken && refreshTokens.add(refreshToken);
  successMsg = responseGenerator(
    (error = false),
    (msg = { accessToken: accessToken, refreshToken: refreshToken })
  );
  return res.status(200).send(successMsg);

  // verify user in the system
  const isValidUser = (user) => {
    return user.username === req.body.username;
  };

  const validUser = users.find(isValidUser);
  if (!validUser) {
    const err = responseGenerator(true, "invalid credentials!");
    return res.status(404).send(err);
  }

  //   verify password
  try {
    const validPassword = await bcrypt.compare(
      req.body.password,
      validUser.password
    );

    if (validPassword) {
      user = { username: req.body.username };
      const accessToken = generateJwtToken(user);
      const refreshToken = generateJwtTokenWithoutExpiration(user);
      refreshToken && refreshTokens.add(refreshToken);
      console.log(refreshTokens);
      successMsg = responseGenerator(
        (error = false),
        (msg = { accessToken: accessToken, refreshToken: refreshToken })
      );
      return res.status(200).send(successMsg);
    } else {
      errorMsg = responseGenerator(
        (error = true),
        (msg = "invalid credentials!")
      );
      return res.status(404).send(errorMsg);
    }
    // catch unhandled exception
  } catch (err) {
    return res.status(500).send(res);
  }
});

// geenrate access token from refresh token
app.post("/token", (req, res) => {
  const refreshToken = req.body.token;
  if (refreshToken == null) return res.sendStatus(401);
  if (!refreshTokens.has(refreshToken)) return res.sendStatus(403);
  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    userdata = { username: user };
    const accessToken = generateJwtToken(userdata);
    successMsg = responseGenerator(false, { accessToken: accessToken });
    res.json(successMsg);
  });
});

app.delete("/logout", (req, res) => {
  refreshTokens.delete(req.body.token);
  res.sendStatus(204);
});

/* ---------------------
start auth server 
------------------------*/
app.listen(4000, () => {
  console.log("started auth server on port 4000");
});
