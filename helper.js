/* 
---------------------
helper functions 
---------------------
*/

const jwt = require("jsonwebtoken");

// api response generator
const responseGenerator = (error, msg) => {
  return { error: error, data: msg };
};

// jwt token generator
const generateJwtToken = (data) => {
  return jwt.sign(data, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: process.env.JWT_EXPIRES,
  });
};

// jwt token without expiration
const generateJwtTokenWithoutExpiration = (data) => {
  return jwt.sign(data, process.env.REFRESH_TOKEN_SECRET);
};

module.exports = {
  generateJwtToken,
  responseGenerator,
  generateJwtTokenWithoutExpiration,
};
