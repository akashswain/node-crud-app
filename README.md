# README

### Pre-requisites

1. Node and npm is installed in the system

### Run the application

1.  Clone the repo
2.  Open terminal and navigate to the root directory

    `cd "<root directory path>"`

3.  create a `.env` file and add below key-value pair

    ```
    ACCESS_TOKEN_SECRET=<secret generated in node terminal: require("crypto").randomBytes(64).toString('hex')>
    REFRESH_TOKEN_SECRET=<secret generated in node terminal: require("crypto").randomBytes(64).toString('hex')>
    JWT_EXPIRES=<time in specific format like 1s, 1m, 1h>
    ```

4.  Install all npm dependencies

    `npm install`

5.  Start the application server in development mode using npm

    `npm run startDev`

6.  Start the authentication server in development mode using npm

    `npm run startDevAuth`

### Test application

1.  Test the application using `server.rest` file

    - Open the file in vscode, click the `send request` link above each api call request.
