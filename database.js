require("dotenv").config();
const Pool = require("pg").Pool;

// get connection from a pool
const pool = new Pool({
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  port: process.env.POSTGRES_PORT,
});

// the pool will emit an error on behalf of any idle clients
// it contains if a backend error or network partition happens
pool.on("error", (err, client) => {
  console.error("Unexpected error on idle client", err);
  process.exit(-1);
});

const create_tables = () => {
  const queries = {
    configure_uuid: `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`,
    users: `
            CREATE TABLE IF NOT EXISTS users (
                id uuid DEFAULT uuid_generate_v4 (),
                username varchar,
                password varchar
            );
            `,

    posts: `
            CREATE TABLE IF NOT EXISTS posts (
              id uuid DEFAULT uuid_generate_v4 (),
              title varchar,
              detail varchar

            );
          `,
  };

  for (const [key, value] of Object.entries(queries)) {
    // console.log(`${key} ----> ${value}`);
    (async () => {
      const client = await pool.connect();
      try {
        const res = await client.query(`${value}`);
        console.log(res);
      } finally {
        // Make sure to release the client before any error handling,
        // just in case the error handling itself throws an error.
        client.release();
      }
    })().catch((err) => console.log(err.stack));
  }
};

create_tables();

users = [];
posts = [
  {
    username: "John Doe",
    title: "Javascript 101",
    detail: "details of the tutorial",
  },
  {
    username: "Zach Jen",
    title: "Javascript 102",
    detail: "details of the tutorial",
  },
];

const refreshTokens = new Set();

module.exports = { users, posts, refreshTokens, pool };
